"""Configuration as defined in 12-factor app pattern.
Maintainer: r@nd0m.bash <bernard.schimmelpfennig@emako.pl>

Imports:
    os.path
    os.environ

Project-independent fields:
    TEST        bool    test mode specifier
                        defaults to ${environ['TEST']}
                        if run in test mode, all IAI orders inserted will be cancelled
                        and no payment will be confirmed; local sqlite db will be used
                        for integration records
    DEBUG       bool    log level specifier
                        defaults to ${environ['DEBUG']}
                        if run with DEBUG, logging level will be set to ${logging.DEBUG}
                        regardless of verbosity flag specified and $api_connector monkey
                        patch will be issued to dump all IAI/Plenty requests
    BASE_DIR    str     path relative to which various filepaths are calculated
                        defaults to ..
    SCRIPT      str     called script name
                        defaults to ${BASE_DIR}.split('/')[-1]
                        useful for $logging.config and future emonitor client
    PIDFILE     str     path to single-process lock
                        defaults to ${environ['PIDFILE']} or ${BASE_DIR}/.${SCRIPT}.lock

Project-specific variables:
    
Logging config fields:
    LOGGING_FILE    str     default logging file
                            defaults to ${environ['LOGGING_DIR']}/${SCRIPT}.log
                                     or ${BASE_DIR}/${SCRIPT}.log
    LOGGING_LEVEL   str     log verbosity level
                            defaults to "DEBUG" if ${DEBUG} evaluates to 1
                                     or ${environ['LOGGING_LEVEL']} or "WARNING"
    LOGGING_CONFIG  dict    logging configuration as defined in logging docs
"""
from os import environ, path

# PROJECT-INDEPENDENT VARIABLES

TEST = environ.get("TEST", "0") == "1"
DEBUG = environ.get("DEBUG", "0") == "1"

BASE_DIR = path.dirname(path.dirname(path.abspath(__file__)))
SCRIPT = path.basename(BASE_DIR)

PIDFILE = environ.get("PIDFILE", path.join(BASE_DIR, f".{SCRIPT}.lock"))

# PROJECT-SPECIFIC VARIABLES


# END PROJECT-SPECIFIC VARIABLES

# LOGGING CONFIG
LOGGING_FILE = path.join(environ.get("LOGGING_DIR", BASE_DIR), f"{SCRIPT}.log")
LOGGING_LEVEL = "DEBUG" if DEBUG else environ.get("LOGGING_LEVEL", "WARNING")
LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "verbose": {
            "format": "[{asctime}] {name:<30} [{levelname:^8}] {message}",
            "style": "{",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
        "terse": {
            "format": "{levelname} {name}: {message}",
            "style": "{",
        },
    },
    "handlers": {
        "console": {
            "level": LOGGING_LEVEL,
            "class": "logging.StreamHandler",
            "formatter": "terse",
        },
        "file": {
            "level": LOGGING_LEVEL,
            "class": "logging.handlers.WatchedFileHandler",
            "filename": LOGGING_FILE,
            "formatter": "verbose",
        },
    },
    "loggers": {
        "": {
            "handlers": [
                "console",
                "file",
            ],
            "level": LOGGING_LEVEL,
            "propagate": False,
        },
        "botocore": {
            "handlers": [],
            "level": "ERROR",
            "propagate": False,
        },
        "urllib3": {
            "handlers": [],
            "level": "ERROR",
            "propagate": False,
        },
    },
}
