from argparse import REMAINDER, ArgumentParser
from os import environ
from typing import Iterable


def create_parser():
    ap = ArgumentParser()

    ap.add_argument(
        "--test", action="store_true", default=None, help="test run indicator"
    )

    ap.add_argument(
        "--logging-dir",
        nargs=1,
        help="directory under which logs are stored (default: ${BASE_DIR}/)",
    )

    ap.add_argument(
        "--pidfile",
        nargs=1,
        help="location of pidfile to use, if any is necessary (default: ${BASE_DIR}/.${SCRIPT}.lock)",
    )

    verbosity = ap.add_mutually_exclusive_group()
    verbosity.add_argument(
        "-s",
        "--silent",
        const="FATAL",
        action="store_const",
        dest="logging_level",
        default="WARNING",
        help="suppress output (why would you possibly do that?!)",
    )
    verbosity.add_argument(
        "-q",
        "--quiet",
        const="ERROR",
        action="store_const",
        dest="logging_level",
        default="WARNING",
        help="only output errors",
    )
    verbosity.add_argument(
        "-v",
        "--verbose",
        const="INFO",
        action="store_const",
        dest="logging_level",
        default="WARNING",
        help="output a lot of useful stuff",
    )
    return ap


# PROJECT-SPECIFIC OPTIONS


def update_parser(ap):
    ap.add_argument("entries", nargs=REMAINDER)


# END PROJECT-SPECIFIC OPTIONS


def parse_args(argv):
    if argv[0].endswith("sh"):
        # removing shell
        argv = argv[1:]
    if argv[0].startswith("python"):
        # removing interpreter
        argv = argv[1:]
    if argv[0] == "-m":
        # removing interpreter args
        argv = argv[1:]

    ap = create_parser()
    update_parser(ap)

    parsed, noparsed = ap.parse_known_args(argv)

    env_update = {}
    for arg, value in parsed._get_kwargs():
        if value is None:
            continue
        if isinstance(value, Iterable) and not isinstance(value, str):
            continue
        if isinstance(value, bool):
            value = int(value)
        env_update[arg.upper()] = str(value)
    environ.update(**env_update)

    return parsed, noparsed
