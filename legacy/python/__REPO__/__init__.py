from logging.config import dictConfig
from sys import argv, exit, stderr

from .args import parse_args

try:
    PARSED_ARGS, NOPARSED_ARGS = parse_args(argv)
except Exception as e:
    stderr.write(f"ARGPARSE ERROR: {e} [{e.__class__.__name__}]\n")
    exit(2)
else:
    from .config import LOGGING_CONFIG

    dictConfig(LOGGING_CONFIG)
