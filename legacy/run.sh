project(){
    local lang="${1}"
    local name="${2##*/}"
    local temp="${TEMPLATE_DIR:-${HOME}/.templates}"
    [[ "$name" ]] || {
        echo "missing project name" >&2
        return 2
    }
    echo "creating ${lang} project '${name}'"
    mkdir -p "${2}/${name}" 
    local src subdir
    for src in "__DEFAULT__" "$lang"; do
        src="${temp}/${src}"
        [[ "$src" ]] || {
            echo "could not find directory $src"
            continue
        }
        for subdir in $(ls -a "$src"); do
            [[ "$subdir" == '.' || "$subdir" == '..' ]] && continue
            subdir="${src}/${subdir}"
            echo "parsing entry $subdir"
            if [[ "$subdir" =~ .*__REPO__ ]]; then
                cp -r "${subdir}"/* "${2}/${name}"
            else
                cp -r "${subdir}" "${2}"
            fi
        done
    done
} 
