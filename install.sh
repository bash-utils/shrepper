#!/bin/bash

prep_dir(){
    echo -n "mkdir -p "${1}":   "
    if ! [ -d "${1}" ]; then 
        mkdir -p "${1}" && echo "OK" || { echo "FAIL"; return 1; }
    fi
}

install(){
    # import configured paths
    source .config/FIELDS || { echo "run ./configure first"; exit 4; }
    prep_dir "${CONFIG_DIR}"     || return 1
    cp -vr etc/* "${CONFIG_DIR}" || return 1
    prep_dir "${INCLUDE_DIR}"    || return 2
    cp -vr bin/* "${INCLUDE_DIR}"|| return 2
    prep_dir "${INSTALL_DIR}"    || return 3
    ln -vs "${INCLUDE_DIR}/main.sh" \
           "${INSTALL_DIR}/shrep"|| return 3
}

clean(){
    rm -ri "${CONFIG_DIR}"
    rm -ri "${INCLUDE_DIR}"
    rm -f  "${INSTALL_DIR}/shrep"
    rm -ri "${INSTALL_DIR}"
}


### MAIN ###

# change to working directory
cd "$(dirname "$0")"

case "${1}" in
    ''|install)
        install || { status=$?; clean; exit ${status}; }
        ;;
    clean|uninstall)
        exit $(clean);
        ;;
esac
