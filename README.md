# shrepper (c) Aachne Dirkschneider <n45t31@protonmail.com>

## version 0.0.1

### About

**shrepper**

### Table of Contents
- [usage](#usage)
  - [synopsis](#synopsis)
  - [options](#options)
  - [exit codes](#exit-status)
  - [examples](#examples)
- [support](#support)
  - [requirements](#prerequisites)
  - [installation](#installation)
- [description](#description)
- [structure](#project-structure)

### Usage
#### Synopsis
**shrep** -h|--version <br/>
**shrep** \[-vqs\] [-C *dir*] -T *type* [-t *target:tags...*] *projectname*

#### Options
###### operation config
<table border="0">
  <tr>
    <td align="right">option</td>
    <td align="center">argument</td>
    <td>specification</td>
  </tr>
</table>

###### console logging
<table border="0">
  <tr>
    <td align="right">-v|--verbose</td>
    <td>outputs lots of useful information</td>
  </tr>
  <tr>
    <td align="right">-q|--quiet</td>
    <td>default, outputs some information</td>
  </tr>
  <tr>
    <td align="right">-s|--silent</td>
    <td>completely suppresses console output</td>
  </tr>
</table>

###### other options
<table border="0">
  <tr>
    <td align="right">-h|--help</td>
    <td>displays help message and exits</td>
  </tr>
  <tr>
    <td align="right">-v|--version</td>
    <td>outputs version string and exits</td>
  </tr>
</table>

#### Exit status
<table border="0">
  <thead>
    <tr>
      <th align="center">NAME</th>
      <th align="center">VALUE</th>
      <th align="center">DESCRIPTION</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td align="right">EXIT_SUCCESS</td>
      <td align="center">0</td>
      <td>shrepper succeeded</td>
    </tr>
    <tr>
      <td align="right">EXIT_FAIL</td>
      <td align="center">1</td>
      <td>shrepper failed</td>
    </tr>
  </tbody>
</table>

#### Examples
- `shrep --version` <br/>displays version string <br/>
- `shrep -h` <br/>displays usage help <br/>

### Support
#### Prerequisites

#### Installation

### Description

### Project structure
